<?PHP
/*******************************************************************************
 * Projekt, Kurs: DT161G
 * File: index.php
 * Desc: Start page for Projekt
 *
 * Mikael Heglind
 * mihe1602
 * mihe1602@student.miun.se
 ******************************************************************************/

include_once 'util.php';

$title = "Twitter2";
$messageMaxSize = Config::getInstance()->getMaxMessageSize();

?>

<!DOCTYPE html>
<html lang="sv-SE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/main.js"></script>
</head>
<body>
<header>
    <img src="img/earth72.png" alt="earth" class="logo"/>
    <div id="pageTitle"><h1><?php echo $title ?></h1></div>
    <div id="login">
        <form id="loginForm">
            <label><b>Username</b></label>
            <input type="text" placeholder="Enter username" name="uname" id="uname"
                   required maxlength="15" value="" autocomplete="off"><br>
            <label><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="psw" id="psw"
                   required><br>
            <button type="button" id="loginButton">Login</button>
            <span id="loginFailure" class="red"></span>
        </form>
        <div id="accountManagement">
            <button type="button" id="registerButton">Register</button>
            <button type="button" id="resetButton">Forgot your password?</button>
        </div>
    </div>
    <div id="logout">
        <p><b id="loginSuccess"></b></p>
        <button type="button" id="logoutButton">Logout</button>
    </div>
</header>
<main>
    <div class="mainContent">
        <div id="readAreaOptions">
            <label for="showUserTextBox">Show user</label>
            <input type="text" id="showUserTextBox">
            <div id="searchWordContainer">
                <label for="searchWordTextBox">Search word</label>
                <input type="text" id="searchWordTextBox">
            </div>
            <div class="sortSelectContainer">
                <label for="sortBySelect"><b>Sort by</b></label>
                <select id="sortBySelect" name="sortBySelect">
                    <option selected>Date</option>
                    <option>Popularity</option>
                </select>
            </div>
        </div>


        <div id="readMessageArea">
        </div>


        <section id="writeMessageSection">
            <div id="writeMessageTextArea"> 
                <form id="messageForm">
                    <label for="messageBody" id="messageAreaLabel">Write message (max <?php echo $messageMaxSize ?> characters)</label>
                    <textarea name="messageBody" id="messageBody"></textarea>
                    <button type="button" id="postButton">Post</button>
                </form>
                <p id="postMessageStatus"></p>
            </div>
        </section>
    </div>



</main>
<footer>
    <div class="footer">
        <p><span class="blue">&copy; Twitter2</span></p>
        <p>by <a href="mailto:mihe1602@student.miun.se">twitter2team</a></p>
        <p>All rights reserved.</p>
    </div>
</footer>

<div id="registrationModal" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="closeModal" id="closeRegistration">&times;</span>
            <h2>Register a new user</h2>
        </div>
        <div class="modal-body">
            <form id="registrationForm">
                <label><b>Username</b></label>
                <input type="text" id="regUsername" placeholder="Enter username"
                       required maxlength="15" value="" autocomplete="off"><br>
                <label><b>Password</b></label>
                <input type="password" id="regPassword" placeholder="Enter Password"
                       required><br>
                <label><b>Confirm password</b></label>
                <input type="password" id="regPswdConfirm" placeholder="Re-enter Password"
                       required><br>
                <label><b>Email adress</b></label>
                <input type="text" id="regEmail" placeholder="Enter a valid email"
                       required><br>
                <button type="button" id="confirmRegistration">Register</button>
                <span id="registrationFailure" class="red"><br></span>
            </form>
        </div>
    </div>
</div>

<div id="resetModal" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="closeModal" id="closeReset">&times;</span>
            <h2>Reset password</h2>
        </div>
        <div class="modal-body">
            <form id="resetForm">
                <label><b>Email adress</b></label>
                <input type="text" id="resetEmail" placeholder="Enter your email address"
                       required><br>
                <button type="button" id="confirmReset">Reset password</button>
                <span id="resetFailure" class="red"><br></span>
                <span id="resetLinkDisplay"></span>
            </form>
        </div>
    </div>
</div>
</body>
</html>