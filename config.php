<?php
/**
 * Created by PhpStorm.
 * User: Mikael
 * Date: 2018-05-14
 * Time: 23:22
 */

// Number of characters allowed in messages
$messageSize = 500;

// Max length of usernames
$usernameSize = 12;

//Allowed characters in usernames
$allowedChars = "#^[a-zA-Z0-9_]+$#";

// Minimum length of of passwords
$passwordMinLength = 9;

//The data needed for login to PostgreSQL account.
$host = 'localhost';          //Address of the database
$port = 5432;                           //Port to use in communication wih database
$dbname = 'postgres';                   //Name of the database
$user = 'postgres';                     //Username on database server
$password = 'dt167g';                //Password on database server
$debug = false;                          //Use debug-mode or not
