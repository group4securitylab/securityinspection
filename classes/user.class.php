<?php
/**
 * Created by PhpStorm.
 * User: Mikael
 * Date: 2018-05-15
 * Time: 23:19
 */

class user {
    private $userId;
    private $username;

    /**
     * User constructor. Creates a User object.
     * @param $username String with username.
     */
    public function __construct($username) {
        $this->username = $username;
    }

    /**
     * Function used for getting access to the functions of the database class.
     * @return database Handle to the database class.
     */
    private function db() {
        return Database::getInstance();
    }


    /**
     * Simple set and get functions of class User.
     */
    public function getUsername() { return $this->username; }

    /**
     * Function checking username and password supplied at login against
     * the database. Caller is notified of the result.
     * @param string The password supplied from login.
     * @return array An array holding messages of result to caller.
     */
    public function isRegistered($password) {
        if ($this->db()->connect()) {
            $isRegistered = $this->db()->checkUser($this, $password);
            $this->db()->disconnect();
            if ($isRegistered)
                return $answer = ['message'=>'Logged in as ' . $this->username, 'result'=>'success'];
            else
                return $answer = ['message'=>'Invalid username or password', 'result'=>'failure'];
        }
        else
            return $answer = ['message'=>'Database failure', 'result'=>'failure'];
    }




}