<?php
/*******************************************************************************
 * Laboration, Kurs: DT167G
 * File: pw_reset.php
 * Desc: Password reset script for the lab assignment
 *
 * Robin Löfås
 * rolo1102
 * rolo1102@student.miun.se
 ******************************************************************************/

include_once 'util.php';

session_start();

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest"){

    $email = html_entity_decode($_POST["email"]);
    $password = $_POST["password"];
    $selector = html_entity_decode($_POST["selector"]);
    $token = html_entity_decode($_POST["token"]);
    $errors = array();
    $resetOk = true;

    $db = Database::getInstance();
    $db->connect();
    $validation = $db->validatePasswordReset($email, $token, $selector);
    $username = $db->getUsernameForEmail($email);

    if (!$validation["success"]) {
        array_push($errors, "Failed to query the database");
        $resetOk = false;
    }
    elseif ($validation["expired"]) {
        array_push($errors, "Reset request expired.");
        $resetOk = false;
    }
    elseif (!$validation["validation"]) {
        array_push($errors, "Validation of the request failed.");
        $resetOk = false;
    }

    if ($resetOk) {

        // Check that password is ok
        $passwordMinLength = $config->getMinimumPasswordLength();
        $passwordOk = true;

        if(strlen($password) <  $passwordMinLength){
            array_push($errors, "Password must be at least $passwordMinLength characters long");
            $passwordOk = false;
        }

        if(!goodPassword($password)){
            array_push($errors, "Password should contain a letter, digit and a special character");
            $passwordOk = false;
        }

        if($password === $username){
            array_push($errors, "Password cannot be the same as the username");
            $passwordOk = false;
        }

        if($db->isPreviousPassword($username, $password)){
            array_push($errors, "Password cannot be the previous password");
            $passwordOk = false;
        }

        if ($passwordOk){
            $result = $db->changePassword($email, $password);

            if(!$result) {
                $resetOk = false;
            }
            else {
                $responseText['result'] = true;
            }
        }
    }

    $db->disconnect();

    if (!$resetOk || !$passwordOk) {
        $responseText["errors"] = $errors;
        $responseText['result'] = false;
    }

    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($responseText);

}

// x-requested-with was not supplied
else {
    exit();
}



?>