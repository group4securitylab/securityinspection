<?php
/*******************************************************************************
 * Laboration, Kurs: DT167G
 * File: register.php
 * Desc: Registration script for the lab assignment
 *
 * Robin Löfås
 * rolo1102
 * rolo1102@student.miun.se
 ******************************************************************************/

include_once 'util.php';

session_start();

$config = Config::getInstance();
$username = $_POST["username"];
$email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
$password = $_POST["password"];
$errors = array();
$registrationOk = true;

// Do password checks here
/**
 * 1. Password should be at least 9 characters long
 * 2. Password is not a dictionary word (have letters, contains number and some special character
 * 2. Password should not be the same as the username
 */

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest"){

    $passwordMinLength = $config->getMinimumPasswordLength();

    if(strlen($password) <  $passwordMinLength){
        array_push($errors, "Password must be at least $passwordMinLength characters long");
        $registrationOk = false;
    }

    if(!goodPassword($password)){
        array_push($errors, "Password should contain a letter, digit and a special character");
        $registrationOk = false;
    }

    if($password === $username){
        array_push($errors, "Password cannot be the same as the username");
        $registrationOk = false;
    }

    //Check ensuring the username contains no forbidden characters.
    if (!preg_match($config->getLegalCharacters(), $username)) {
        array_push($errors, "Username may only contain alphanumeric characters and underscores(_)");
        $registrationOk = false;
    }

    //Check ensuring the username is within allowed size limitations.
    if (strlen($username) > $config->getUsernameLimit()) {
        array_push($errors, "Username may not be longer than 12 characters");
        $registrationOk = false;
    }

    //Check ensuring the email is correctly formatted.
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        array_push($errors, "Invalid email format");
        $registrationOk = false;
    }

    if ($registrationOk) {
        $db = Database::getInstance();
        $db->connect();

        //Check that neither the supplied username nor email are already in the db
        $result = $db->usernameExists($username);

        if ($result['success'] == false) {
            array_push($errors, "Failed to query the database.");
            $registrationOk = false;
        }
        elseif ($result['userFound'] == true) {
            $registrationOk = false;
        }

        $result = $db->emailInUse($email);

        if ($result['success'] == false) {
            array_push($errors, "Failed to query the database.");
            $registrationOk = false;
        }
        elseif ($result['emailFound'] == true) {
            $registrationOk = false;
        }

        if ($registrationOk) {
            $result = $db->addUser($username, $email, $password);

            if(!$result) {
                array_push($errors, "Database error, something went wrong adding new user to the database");
                $registrationOk = false;
            }
    } else {
        array_push($errors, "Registration refused!");
        }

        $db->disconnect();

    }

    if ($registrationOk) {
        $responseText['result'] = true;
    }
    else {
        $responseText['result'] = false;
        $responseText['errors'] = $errors;
    }

    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($responseText);
}

// The x-requested-with header was not included
else {
    exit();
}
?>