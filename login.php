<?php
/**
 * Created by PhpStorm.
 * User: Mikael
 * Date: 2018-05-14
 * Time: 23:14
 */

include_once 'util.php';

// Default response text, user not logged in, empty message
$responseText['loggedIn'] = false;
$responseText['displayMessage'] = '';
$responseText['username'] = '';
$responseText['messages'] = '';


session_start();

/**
 * This code section is entered if the user is already logged in. Links appropriate
 * for the access level of the user is fetched and returned to js for display. A
 * logged in message is returned to js to replace the login boxes.
 */
if (isset($_SESSION['loggedIn']) && isset($_SESSION['user'])) {
    $username = $_SESSION['user']->getUsername();
    $responseText['loggedIn'] = true;
    $responseText['displayMessage'] = 'Logged in as ' . $username;
    $responseText['username'] = $username;
}

/**
 * This section is entered if data from the login field is included in a POST-request.
 * Received data is checked against the database and if credentials are valid the user
 *is logged in. Else an message about incorrect login information is returned to js.
 */
else if (isset($_POST['login'])) {
    if (!isset($_SESSION['loggedIn'])) {
        $user = new User(sanitize($_POST['uname']));
        $answer = $user->isRegistered($_POST['psw']);
        if ($answer['result'] === 'success') {
            $_SESSION['loggedIn'] = true;
            $_SESSION['user'] = $user;
            $responseText['loggedIn'] = true;
            $responseText['displayMessage'] = $answer['message'];
            $responseText['username'] = $_POST['uname'];
        }
        else {
            $responseText['displayMessage'] = $answer['message'];
        }
    }
}
$db = Database::getInstance();
if ($db->connect()) {
    $responseText['messages'] = $db->getAllMessages($responseText['username']);
    $db->disconnect();
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($responseText);