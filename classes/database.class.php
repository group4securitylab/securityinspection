<?php
/**
 * Created by PhpStorm.
 * User: Mikael
 * Date: 2018-05-16
 * Time: 00:15
 */


/**
 * Class for handling all communication with the databas. Functions have been
 * grouped into those concerned with setting up and closing the connection and
 * those mostly concerned with processing user data, category data and image
 * data. Finally there is a small group of utility functions used internally
 * only.
 */
class Database {
    private $db;
    private $dsn;
    private static $instance;


    /**
     * Database class singleton constructor.
     */
    private function __construct() {
        $config = Config::getInstance();
        $this->dsn = $config->getDbDsn();
    }


    /**
     * The singleton instance controller. Makes sure only one instance of the class
     * is used by the application.
     * @return self A new or the already existing instance of the class.
     */
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }


    /**
     * Function for making a connection to the data base.
     * @return boolean true if connection was successful, false otherwise.
     */
    public function connect() {
        if ($this->db = pg_connect($this->dsn))
            return true;
        return false;
    }

    /**
     * Function for closing the connection to the database.
     * @return boolean true if closing was successful, false otherwise.
     */
    public function disconnect() {
        if (pg_close($this->db))
            return true;
        return false;
    }

    /**
     * Function for retrieving all messages from the database.
     * @param $user String for the username if a use is logged in.
     * @return array|null mixed An array of messages or false if no messages in database.
     */
    public function getAllMessages($user) {
        $allMessages = null;
        $sql = 'SELECT * FROM dt167g_lab.message;';
        $messages = pg_query($this->db, $sql);
        if (pg_num_rows($messages) > 0) {
            foreach (pg_fetch_all($messages) as $message) {
                $username = $this->getUsernameByMessageId($message['message_id']);
                $voteCount = $this->getMessageVotes($message['message_id']);
                $datetime = date('Y-m-d H:i:s', strtotime($message['timestamp']));
                $vote = $this->hasVoted($user, $message['message_id']);
                $allMessages[] = ['author'=>$username, 'message_id'=>$message['message_id'], 'message'=>$message['message'],
                    'datetime'=>$datetime, 'voteCount'=>$voteCount, 'userVote'=>$vote];

            }
        }
        return $allMessages;
    }

    /**
     * Fetches a specific message based on messageId.
     * @param $id Integer for the messageId
     * @return array|bool mixed Return the message or false if no message is found.
     */
    public function getMessageById($id) {
        $sql = 'SELECT * FROM dt167g_lab.message WHERE message_id = $1;';
        $result = pg_query_params($this->db, $sql, array($id));

        $response = false;
        if ($result && pg_affected_rows($result) >0 ) {
            $message = pg_fetch_all($result)[0];

            $username = $this->getUsernameByMessageId($message['message_id']);
            $voteCount = $this->getMessageVotes($message['message_id']);
            $datetime = date('Y-m-d H:i:s', strtotime($message['timestamp']));
            $response = ['author'=>$username, 'message_id'=>$message['message_id'], 'message'=>$message['message'],
                'datetime'=>$datetime, 'voteCount'=>$voteCount];
        }

        return $response;
    }

    /**
     * Fethes the auhor of a specific message.
     * @param $messageId Integer for the message id
     * @return string The message.
     */
    public function getUsernameByMessageId($messageId) {
        $sql = 'SELECT "user".username FROM dt167g_lab."user", dt167g_lab.user_message WHERE ' .
            ' "user".user_id=user_message.user_id AND user_message.message_id=$1';
        $result = pg_query_params($this->db,$sql, array((int)$messageId));
        return pg_fetch_result($result, 0);
    }

    /**
     * Counts the sum of total votes of specific message.
     * @param $message_id Integer for the id of the message.
     * @return int The vote count.
     */
    private function getMessageVotes($message_id) {
        $sql = 'SELECT vote FROM dt167g_lab.user_votes WHERE message_id=' . (int)$message_id;
        $votes = pg_query($this->db, $sql);
        if (pg_num_rows($votes) === 0)
            return 0;
        else {
            $voteSum = 0;
            foreach (pg_fetch_all_columns($votes) as $vote)
                $voteSum += $vote;
            return $voteSum;
        }
    }


    /**
     * Function checking a password from login event against password stored in
     * database. If passwords match data for the user is fetched and returned.
     * If passwords do not match the received user object is returned without
     * filled in data.
     * @param string A new incomplete User object.
     * @param string The password entered in the login attempt.
     * @return boolean A filled in or incomplete UserObject
     */
    public function checkUser($user, $password) {
        $params = array($user->getUsername());
        $sql = 'SELECT password FROM dt167g_lab.user WHERE username=$1;';
        if (pg_num_rows($result = pg_query_params($this->db, $sql, $params)) === 1) {
            if (password_verify($password, pg_fetch_result($result, 0, 0))) {
                return true;
            }
        }
        return false;
    }


    /**
     * This function deletes a message with the given id and returns true if it
     * was successful.
     *
     * @param string $messageId - the id of the message that should be deleted
     * @return bool- true if message is deleted, else false.
     */
    public function deleteMessage($messageId) {

        $sql = "DELETE FROM dt167g_lab.message WHERE message_id = $1";
        $result = pg_query_params($this->db, $sql, array($messageId));
        $this->deleteMessageVotes($messageId);

        if ($result)
            return true;

        return false;
    }

    /**
     * On delete message, this function deletes related posts in user_votes table.
     * @param $messageId integer MessageId number.
     */
    function deleteMessageVotes($messageId) {
        if ($this->getMessageVotes($messageId) > 0) {
            $sql = "DELETE FROM dt167g_lab.user_votes WHERE message_id = $1";
            $result = pg_query_params($this->db, $sql, array($messageId));
        }
    }

    /**
     * Checks if a user has voted on a specific message.
     * @param $username String the user.
     * @param $messageId Integer for the message id.
     * @return bool|int mixed returns the vote or false if has not voted.
     */
    public function hasVoted($username, $messageId) {
        if ($username != '') {
            if ($id = $this->getUserIdByUsername($username)) {
                $sql = 'SELECT vote FROM dt167g_lab.user_votes WHERE user_id=$1 AND message_id=$2;';
                if ($vote = pg_query_params($this->db, $sql, array($id, $messageId))) {
                    if (pg_num_rows($vote) === 1) {
                        return (int)pg_fetch_result($vote, 0);
                    }
                }
            }
        }
        return false;
    }

    /**
     * Retrieving the user id based on user name.
     * @param $username String for the user.
     * @return bool|int mixed The userId or false if no id if found.
     */
    private function getUserIdByUsername($username) {
        $params = array($username);
        $sql = 'SELECT user_id FROM dt167g_lab.user WHERE username=$1;';
        if ($id = pg_query_params($this->db, $sql, $params)) {
            return (int)pg_fetch_result($id, 0);
        }
        else
            return false;
    }

    /**
     * Retrieving the user id based on email address.
     * @param $email -  email of the user.
     * @return bool|int mixed The userId or false if no id if found.
     */
    private function getUserIdByEmail($email) {
        $params = array($email);
        $sql = 'SELECT user_id FROM dt167g_lab.user WHERE email=$1;';
        if ($id = pg_query_params($this->db, $sql, $params)) {
            return (int)pg_fetch_result($id, 0);
        }
        else
            return false;
    }

    public function addMessage($username, $text) {
        // Get user id
        $sql = "SELECT user_id FROM dt167g_lab.user WHERE username = $1";
        $result = pg_query_params($this->db, $sql, array($username));
        if ($result && pg_affected_rows($result) > 0)
            $userId = pg_fetch_all_columns($result, 0)[0];
        else {
            // user not found
            return array('success' => false, 'message' => "[db error: user not found]");
        }


        // add message
        $sql = "INSERT INTO dt167g_lab.message (message) VALUES ($1) RETURNING message_id";
        $result = pg_query_params($this->db, $sql, array($text));

        // if message was added, link it to a user
        if ($result && pg_affected_rows($result) > 0) {
            $messageId = pg_fetch_all_columns($result, 0)[0];

            // add link
            $sql = "INSERT INTO dt167g_lab.user_message (user_id, message_id) VALUES ($1, $2)";
            $result = pg_query_params($this->db, $sql, array($userId, $messageId));

            // Check that link was added
            if ($result && pg_affected_rows($result) > 0) {
                // link was added
                return array('success' => true, 'message' => "[db message: message added]", 'messageId' => $messageId);
            }
            else {
                // failed to add link. Roll back by deleting message
                $result = pg_delete($this->db, 'dt167g_lab.message', array('message_id' => $messageId));

                // check that message was deleted
                if ($result) {
                    // message deleted
                    return array('success' => false, 'message' => "[db error: failed to add message]");
                }
                else {
                    // rollback failed
                    return array('success' => false, 'message' => "[db critical error: rollback failed]");
                }
            }
        }
        else {
            // failed to add message
            return array('success' => false, 'message' => "[db error: failed to add message]");
        }
    }

    /**
     * Checks if a username is in the db
     *
     * @param string $username - username to be searched for in the db
     * @return array - array containing boolean values indicating the result of the query
     */
    public function usernameExists($username) {
        $params = array($username);
        $sql = 'SELECT * FROM dt167g_lab.user WHERE username=$1';
        $result = pg_query_params($this->db, $sql, $params);

        if($result) {
            if (pg_affected_rows($result) > 0) {
                return array('success' => true, 'userFound' => true);
            }
            else {
                return array('success' => true, 'userFound' => false);
            }

        }
        else {
            return array('success' => false);
        }
    }

    /**
     * Checks if an email address is in the db
     *
     * @param string $email - email to be searched for in the db
     * @return array - array containing boolean values indicating the result of the query
     */
    public function emailInUse($email) {
        $params = array($email);
        $sql = 'SELECT * FROM dt167g_lab.user WHERE email=$1';
        $result = pg_query_params($this->db, $sql, $params);

        if($result) {
            if (pg_affected_rows($result) > 0) {
                return array('success' => true, 'emailFound' => true);
            }
            else {
                return array('success' => true, 'emailFound' => false);
            }

        }
        else {
            return array('success' => false);
        }
    }

    /**
     * Adds a user to the db.
     *
     * @param string $username - desired username
     * @param string $email - email address of the user
     * @param string $password - desired password
     * @return bool - true if successful, false if not
     */
    public function addUser($username, $email, $password) {
        $params = array($username, $email, password_hash($password, PASSWORD_BCRYPT));
        $sql = "INSERT INTO dt167g_lab.user (username, email, password) VALUES ($1, $2, $3)";
        $result = pg_query_params($this->db, $sql, $params);

        if($result) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Gets username for e-mail string.
     *
     * @param string $email - email address of the user
     * @return string - user username or null on fail.
     */
    public function getUsernameForEmail($email) {
        $params = array($email);
        $sql = "SELECT username FROM dt167g_lab.user WHERE email = $1";
        $result = pg_query_params($this->db, $sql, $params);

        if($result) {
            $usernameRow = pg_fetch_result($result, 0);
            pg_free_result($result);
            return $usernameRow[0];
        }
        else {
            return null;
        }
    }

    /**
     * Checks if a supplied password is the current one.
     * @param $username string Username of the user.
     * @param $password string Password of the user.
     * @return bool If the password is the same.
     */
    public function isPreviousPassword($username, $password){
        $params = array($username);
        $sql = "SELECT password FROM dt167g_lab.user WHERE username = $1";
        $result = pg_query_params($this->db, $sql, $params);

        if($result) {
            $passwordRow = (string)pg_fetch_result($result, 0);
            pg_free_result($result);

            return password_verify($password, $passwordRow);
        }
        else {
            return true;
        }
    }

    /**
     * Creates an entry in the database that a password reset has been requested for a specific user. Generates
     * a token and selector to be used for that purpose.
     *
     * @param $email - Email of the user
     * @return array - contains a bool value indicating the success or failure of the queries performed. On success also returns
     * string variables holding the token and selector.
     * @throws Exception
     */
    public function schedulePasswordReset($email) {
        $id = $this->getUserIdByEmail($email);

        if (!$id) {
            return array("success" => false);
        }

        //Removes any previously scheduled reset
        $params = array($id);
        $sql = 'DELETE FROM dt167g_lab.reset_requests WHERE user_id=$1';
        $result = pg_query_params($this->db, $sql, $params);

        if(!$result) {
            return array("success" => false);
        }

        $selector = bin2hex(random_bytes(8));
        $token = bin2hex(random_bytes(32));
        // Sets an expiration time for the request
        $expiration = new DateTime('NOW');
        $expiration->add(new DateInterval('PT24H'));
        $params = array($id, $selector, hash("sha256", $token), date_timestamp_get($expiration));
        $sql = 'INSERT INTO dt167g_lab.reset_requests (user_id, selector, token, expiration) VALUES ($1, $2, $3, $4)';
        $result = pg_query_params($this->db, $sql, $params);

        if(!$result) {
            return array("success" => false);
        }
        else {
            return array("success" => true, "token" => $token, "selector" => $selector);
        }
    }

    /**
     * Checks that a password request has been scheduled for a specific user and checks the supplied parameters against
     * the db entry in question. Also checks the timestamp stored in the db against the current time and fails to validate
     * if more than 24 hours has passed.
     *
     * @param $email - email of the user
     * @param $token
     * @param $selector
     * @return array
     */
    public function validatePasswordReset($email, $token, $selector) {
        $id = $this->getUserIdByEmail($email);

        if (!$id) {
            return array("success" => false);
        }

        $params = array($id);
        $sql = 'SELECT * FROM dt167g_lab.reset_requests WHERE user_id=$1';
        $result = pg_query_params($this->db, $sql, $params);

        if (!$result) {
            return array("success" => false);
        }
        else {
            $request = pg_fetch_object($result, 0);
            $time = new DateTime('NOW');

            //
            if (date_timestamp_get($time) > $request->expiration) {
                return array("success" => true, "validation" => false, "expired" => true);
            }

            if ($request->selector != $selector) {
                return array("success" => true, "validation" => false, "expired" => false);
            }
            else {
                if ($request->token == hash("sha256", $token)) {
                    return array("success" => true, "validation" => true, "expired" => false);
                }
                else {
                    return array("success" => true, "validation" => false, "expired" => false);
                }
            }

        }
    }

    /**
     * Updates the password stored in the db for a specific user.
     *
     * @param $email - email of the user
     * @param $newPassword - new password value
     * @return bool - false if any query fails otherwise true
     */
    public function changePassword($email, $newPassword) {
        $params = array(password_hash($newPassword, PASSWORD_BCRYPT), $email);
        $sql = 'UPDATE dt167g_lab.user SET password = $1 WHERE email = $2';
        $result = pg_query_params($this->db, $sql, $params);

        if (!$result) {
            return false;
        }
        else {
            //Removes the request from the database on successful password change
            $id = $this->getUserIdByEmail($email);
            $params = array($id);
            $sql = 'DELETE FROM dt167g_lab.reset_requests WHERE user_id=$1';

            if(pg_query_params($this->db, $sql, $params)) {
                return true;
            }
            else {
                return false;
            }

        }
    }


    public function voteMessage($userVoting, $msgId, $vote) {
        $author = false;

        // Check that the message exists by getting the author of the message
        $sql = 'SELECT username FROM dt167g_lab.user 
                INNER JOIN dt167g_lab.user_message 
                ON dt167g_lab.user.user_id = dt167g_lab.user_message.user_id 
                INNER JOIN dt167g_lab.message 
                ON dt167g_lab.message.message_id = dt167g_lab.user_message.message_id 
                WHERE dt167g_lab.message.message_id = $1;';
        $result = pg_query_params($this->db, $sql, array($msgId));

        if ($result && pg_num_rows($result) > 0)
            $author = pg_fetch_all_columns($result, 0)[0];

        // if message exist and the user that tries to vote isn't the author of the message
        if ($author !== $userVoting) {
            // add vote
            $userId = $this->getUserIdByUsername($userVoting);
            $sql = 'INSERT INTO dt167g_lab.user_votes (user_id, message_id, vote) VALUES ($1,$2,$3)';
            $result = pg_query_params($this->db, $sql, array($userId, $msgId, $vote));

            // If insertion succeeded
            if ($result && pg_affected_rows($result) > 0) {
                return true;
            }
        }
        return false;
    }
}

