<?php
/**
 * Created by PhpStorm.
 * User: dreadflopp
 * Date: 2018-05-22
 * Time: 14:50
 */

include_once 'util.php';

// Default response text
$responseText['success'] = false;
$responseText['messageId'] = '';
$responseText['displayMessage'] = '';
$responseText['newVote'] = '';

session_start();

// Check that request was sent with XMLHttpRequest
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest"){

    // Check that user is logged in
    if (isset($_SESSION['loggedIn']) && isset($_SESSION['user'])) {
        if (isset($_POST['msgId']) && isset($_POST['vote'])) {
            if ($_POST['vote'] == -1 || $_POST['vote'] = 1) {
                $newVote = false;
                $db = Database::getInstance();
                if ($db->connect()) {
                    $username = $_SESSION['user']->getUsername();
                    $newVote = $db->voteMessage($username, $_POST['msgId'], $_POST['vote']);
                    $db->disconnect();
                }

                if ($newVote == false) {
                    $responseText['success'] = false;
                    $responseText['messageId'] = '';
                    $responseText['displayMessage'] = 'Failed to vote';

                } else {
                    $responseText['success'] = true;
                    $responseText['messageId'] = $_POST['msgId'];
                    $responseText['displayMessage'] = 'Voting succeeded';
                    $responseText['newVote'] = $newVote;
                }
            }
        }
    }
}



header('Content-Type: application/json; charset=utf-8');
echo json_encode($responseText);