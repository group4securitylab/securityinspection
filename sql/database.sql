-- ##############################################
-- KURS: dt167g_lab
-- Lab
-- Mattias Lindell
-- Create table called member
-- Create table called yyy
-- etc...................................
-- ##############################################

-- First we create the schema

DROP SCHEMA IF EXISTS dt167g_lab CASCADE;

CREATE SCHEMA dt167g_lab;



-- First we create the member table
DROP TABLE IF EXISTS dt167g_lab.user;

CREATE TABLE dt167g_lab.user (
  user_id     SERIAL PRIMARY KEY,
  username    text NOT NULL CHECK (username <> ''),
  email       text NOT NULL CHECK (email <> ''),
  password    text NOT NULL CHECK (password  <> ''),
  CONSTRAINT unique_member UNIQUE(username)
)
WITHOUT OIDS;

INSERT INTO dt167g_lab.user (username, email, password) VALUES ('a', 'a@a.com', '$2y$10$t7GiPezaVo1vLjAJCOvP2ujZ2kvKPh5RhiNmt2BaFpkY/amdEChyW');
INSERT INTO dt167g_lab.user (username, email, password) VALUES ('b', 'b@b.com', '$2y$10$xk40vfflcDaFzXnW.UZjtuve615w8osy70OLBVJr8M6gEFQzo45OW');



-- Then we create the message table
DROP TABLE IF EXISTS dt167g_lab.message;

CREATE TABLE dt167g_lab.message (
  message_id    SERIAL PRIMARY KEY,
  message       text NOT NULL CHECK (message <> ''),
  timestamp     timestamp with time zone NOT NULL DEFAULT now(),
  CONSTRAINT unique_message UNIQUE(message_id)
)
WITHOUT OIDS;

INSERT INTO dt167g_lab.message(message) VALUES ('first message');
INSERT INTO dt167g_lab.message(message) VALUES ('second message');



-- Then we create the member_message table

DROP TABLE IF EXISTS dt167g_lab.user_message;

CREATE TABLE dt167g_lab.user_message (
  user_id      integer REFERENCES dt167g_lab.user (user_id),
  message_id   integer REFERENCES dt167g_lab.message (message_id) ON DELETE CASCADE,
  PRIMARY KEY (user_id, message_id),
  CONSTRAINT unique_user_message UNIQUE(user_id, message_id)
)
WITHOUT OIDS;

INSERT INTO dt167g_lab.user_message(user_id, message_id) VALUES (1,1);
INSERT INTO dt167g_lab.user_message(user_id, message_id) VALUES (2,2);



-- Then we create the votes table

DROP TABLE IF EXISTS dt167g_lab.user_votes;

CREATE TABLE dt167g_lab.user_votes (
  user_id      integer REFERENCES dt167g_lab.user (user_id),
  message_id   integer REFERENCES dt167g_lab.message (message_id) ON DELETE CASCADE,
  vote         integer NOT NULL CHECK (vote >= -1) CHECK (vote <= 1) CHECK (vote <> 0),
  CONSTRAINT unique_votes UNIQUE(user_id, message_id)
)
WITHOUT OIDS;

--INSERT INTO dt167g_lab.user_votes (user_id, message_id, vote) VALUES (1,1,1);
--INSERT INTO dt167g_lab.user_votes (user_id, message_id, vote) VALUES (2,2,1);
INSERT INTO dt167g_lab.user_votes (user_id, message_id, vote) VALUES (1,2,-1);
INSERT INTO dt167g_lab.user_votes (user_id, message_id, vote) VALUES (2,1,1);

DROP TABLE IF EXISTS dt167g_lab.reset_requests;

CREATE TABLE dt167g_lab.reset_requests (
  user_id      integer REFERENCES dt167g_lab.user (user_id),
  selector     character(16) NOT NULL,
  token        character(64) NOT NULL,
  expiration   bigint NOT NULL
)