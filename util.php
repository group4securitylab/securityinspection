<?php
/**
 * Created by PhpStorm.
 * User: Mikael
 * Date: 2018-05-14
 * Time: 23:20
 */



function my_autoloader($class) {
    $classfilename = strtolower($class);
    include 'classes/' . $classfilename . '.class.php';
}
spl_autoload_register('my_autoloader');


$config = Config::getInstance();

/**
 * set debug true/false to change php.ini
 * To get more debug information when developing set to true,
 * for production set to false
 */
if ($config->checkDebug()) {
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
}


function sanitize($userInput) {
    $trimmedString = trim($userInput);
    $sanitizedString = filter_var($trimmedString, FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW);
    return $sanitizedString;
}

/**
 * Function to verify password complexity
 * @param String $pwd the password
 * @return bool true if password has letter, digit and special character, else false
 */
function goodPassword(String $pwd): bool{
    if(!is_null($pwd)){
        $containsLetter = preg_match('/[a-zA-Z]/', $pwd);
        $containsDigit = preg_match('/\d/', $pwd);
        $containsSpecialChar = preg_match('/[^a-zA-Z\d]/', $pwd);

        return $containsDigit && $containsLetter && $containsSpecialChar;
    }
    return false;
}

/*
 FILTER_FLAG_STRIP_LOW
FILTER_FLAG_STRIP_HIGH
FILTER_FLAG_ENCODE_AMP

$postfilter =    // set up the filters to be used with the trimmed post array
    array(
        'user_tasks'                        =>    array('filter' => FILTER_SANITIZE_STRING, 'flags' => !FILTER_FLAG_STRIP_LOW),    // removes tags. formatting code is encoded -- add nl2br() when displaying
        'username'                            =>    array('filter' => FILTER_SANITIZE_ENCODED, 'flags' => FILTER_FLAG_STRIP_LOW),    // we are using this in the url
        'mod_title'                            =>    array('filter' => FILTER_SANITIZE_ENCODED, 'flags' => FILTER_FLAG_STRIP_LOW),    // we are using this in the url
    );
*/