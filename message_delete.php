<?php
/**
 * Created by PhpStorm.
 * User: mattias
 * Date: 2018-05-17
 * Time: 17:21
 */

include_once 'util.php';

// Default response text
$responseText['success'] = false;
$responseText['messageId'] = '';
$responseText['displayMessage'] = '';

session_start();

// delete a message
if ($_SERVER["REQUEST_METHOD"] == "POST"){

    // Decodes JSON from POST
    $input = json_decode(file_get_contents('php://input'));

    // Check that input is not empty
    if (!empty($input->task) && !empty($input->id)){

        if (isset($_SESSION['loggedIn']) && isset($_SESSION['user'])) {
            // Check who the creator of the message with the given id is
            $db = Database::getInstance();
            if ($db->connect())
                $creatorName = $db->getUsernameByMessageId($input->id);
            $db->disconnect();

            // if the creator of the message is the logged in user, delete the message
            if ($creatorName === $_SESSION['user']->getUsername()) {
                if ($db->connect() && $db->deleteMessage($input->id)) {
                    $responseText['success'] = true;
                    $responseText['messageId'] = $input->id;
                    $responseText['displayMessage'] = 'The message was successfully deleted';
                }
                $db->disconnect();
            }
        }

    }
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($responseText);